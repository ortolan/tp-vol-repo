package tp.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import tp.vol.model.Reservation;

public class TestReservation {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Reservation res = new Reservation();
		res.setNumero("Res1");
		res.setStatut(true);
		res.setTarif(426.4f);
		res.setTauxTVA(1);
		try {
			res.setDateReservation(sdf.parse("19/06/2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		SingletonVol.getInstance().getReservationDao().create(res);

		res = SingletonVol.getInstance().getReservationDao().findById("Res1");

		System.out.println(res.getNumero() + " " + res.getTarif());

		
		res.setTarif(32);

		SingletonVol.getInstance().getReservationDao().update(res);

		res = SingletonVol.getInstance().getReservationDao().findById("Res1");

		System.out.println(res.getNumero() + " " + res.getTarif() + " " + res.getDateReservation());

		SingletonVol.getInstance().getReservationDao().delete(res);

		res = SingletonVol.getInstance().getReservationDao().findById("Res1");

		System.out.println(res);

	}

}
