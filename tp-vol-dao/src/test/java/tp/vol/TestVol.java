package tp.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import tp.vol.model.Compagnie;
import tp.vol.model.Vol;

public class TestVol {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Compagnie fluck = new Compagnie();
		fluck.setNom("Fluck");
		
		SingletonVol.getInstance().getCompagnieDao().create(fluck);
				
		Vol vol1 = new Vol();
		vol1.setNumero("TB6");
		try {
			vol1.setDateDepart(sdf.parse("19/06/2019"));
			vol1.setDateArrivee(sdf.parse("19/06/2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		vol1.setOuvert(true);
		vol1.setNbPlace(69);
		vol1.setCompagnie(fluck);

		SingletonVol.getInstance().getVolDao().create(vol1);

		vol1 = SingletonVol.getInstance().getVolDao().findById("TB6");

		System.out.println(vol1.getNumero() + " " + vol1.getNbPlace());

		vol1.setNbPlace(666);

		SingletonVol.getInstance().getVolDao().update(vol1);

		vol1 = SingletonVol.getInstance().getVolDao().findById("TB6");

		System.out.println(vol1.getNumero() + " " + vol1.getNbPlace() + " " + vol1.getDateDepart());

		SingletonVol.getInstance().getVolDao().delete(vol1);

		vol1 = SingletonVol.getInstance().getVolDao().findById("TB6");

		System.out.println(vol1);
	}

}
