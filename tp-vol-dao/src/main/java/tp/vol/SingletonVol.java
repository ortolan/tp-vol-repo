package tp.vol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import tp.vol.dao.IAeroportDao;
import tp.vol.dao.IClientDao;
import tp.vol.dao.ICompagnieDao;
import tp.vol.dao.IPassagerDao;
import tp.vol.dao.IReservationDao;
import tp.vol.dao.IVilleDao;
import tp.vol.dao.IVolDao;
import tp.vol.dao.IVoyageDao;
import tp.vol.dao.sql.AeroportDaoSql;
import tp.vol.dao.sql.ClientDaoSql;
import tp.vol.dao.sql.CompagnieDaoSql;
import tp.vol.dao.sql.PassagerDaoSql;
import tp.vol.dao.sql.ReservationDaoSql;
import tp.vol.dao.sql.VilleDaoSql;
import tp.vol.dao.sql.VolDaoSql;
import tp.vol.dao.sql.VoyageDaoSql;

public class SingletonVol {

	private static SingletonVol instance = null;
//
	private final IVilleDao villeDao = new VilleDaoSql();
	private final IClientDao clientDao = new ClientDaoSql();
	private final IReservationDao reservationDao = new ReservationDaoSql();
	private final IPassagerDao passagerDao = new PassagerDaoSql();

	private final IAeroportDao aeroportDao = new AeroportDaoSql();
	
private final ICompagnieDao compagnieDao = new CompagnieDaoSql();
	private final IVoyageDao voyageDao = new VoyageDaoSql();
	private final IVolDao volDao = new VolDaoSql();

	private SingletonVol() {
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static SingletonVol getInstance() {
		if (instance == null) {
			instance = new SingletonVol();
		}

		return instance;
	}

	public IClientDao getClientDao() {
		return clientDao;
	}

//
	public IPassagerDao getPassagerDao() {
		return passagerDao;
	}

	public IReservationDao getReservationDao() {
		return reservationDao;
	}

//
	public IVilleDao getvilleDao() {
		return villeDao;
	}

	public IAeroportDao getAeroportDao() {
			return aeroportDao;
		}
	public IVolDao getVolDao() {
		return volDao;
	}
	
	public IVoyageDao getVoyageDao() {
		return voyageDao;
	}
	public ICompagnieDao getCompagnieDao() {
		return compagnieDao;
	}



	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "vol", "vol");
	}
	
}