package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.SingletonVol;
import tp.vol.dao.IClientDao;
import tp.vol.model.Civilite;
import tp.vol.model.Client;
import tp.vol.model.ClientParticulier;
import tp.vol.model.ClientPro;
import tp.vol.model.MoyenPaiement;
import tp.vol.model.TypeClient;

public class ClientDaoSql implements IClientDao {

	@Override
	public List<Client> findAll() {
		List<Client> clients = new ArrayList<Client>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT client_id, type_client, "
					+ "moyent_paiement, civilite, nom, prenom, numero_siret, "
					+ "nom_entreprise, numero_tva, type_entreprise, mail, telephone " + " FROM clients");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String typeClient = rs.getString("type_client");
				String moyenPaiement =rs.getString("moyent_paiement");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				int siret = rs.getInt("numero_siret");
				String nomEntreprise = rs.getString("nom_entreprise");
				String numeroTva = rs.getString("numero_tva");
				String typeEntreprise =rs.getString("type_entreprise");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");				
				
				while (rs.next()) {
				if (typeClient == "Particulier") {

					ClientParticulier particulier = new ClientParticulier();
						particulier.setTypeClient(TypeClient.valueOf(typeClient));
						particulier.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
						particulier.setCivilite(Civilite.valueOf(civilite));
						particulier.setNom(nom);
						particulier.setPrenoms(prenom);
						particulier.setMail(mail);
						particulier.setTelephone(telephone);	

						clients.add(particulier);
				}
				}
				
				if (typeClient == "Professionnel") {

					ClientPro pro = new ClientPro();
						pro.setTypeClient(TypeClient.valueOf(typeClient));
						pro.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
						pro.setNumeroSIRET(siret);
						pro.setNomEntreprise(nomEntreprise);
						pro.setTypeEntreprise(typeEntreprise);

						clients.add(pro);
				
			}
			}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			return (List<Client>) clients;
		}

	@Override
	public Client findById(Long id) {
		List<Client> clients = new ArrayList<Client>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT client_id, type_client, "
					+ "moyent_paiement, civilite, nom, prenom, numero_siret, "
					+ "nom_entreprise, numero_tva, type_entreprise, mail, telephone " + " FROM clients WHERE client_id = ?");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String typeClient = rs.getString("type_client");
				String moyenPaiement =rs.getString("moyent_paiement");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				int siret = rs.getInt("numero_siret");
				String nomEntreprise = rs.getString("nom_entreprise");
				String numeroTva = rs.getString("numero_tva");
				String typeEntreprise =rs.getString("type_entreprise");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");				
				
				while (rs.next()) {
				if (typeClient == "Particulier") {

					ClientParticulier particulier = new ClientParticulier();
						particulier.setTypeClient(TypeClient.valueOf(typeClient));
						particulier.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
						particulier.setCivilite(Civilite.valueOf(civilite));
						particulier.setNom(nom);
						particulier.setPrenoms(prenom);
						particulier.setMail(mail);
						particulier.setTelephone(telephone);	

						clients.add(particulier);
				}
				}
				
				if (typeClient == "Professionnel") {

					ClientPro pro = new ClientPro();
						pro.setTypeClient(TypeClient.valueOf(typeClient));
						pro.setMoyenPaiement(MoyenPaiement.valueOf(moyenPaiement));
						pro.setNumeroSIRET(siret);
						pro.setNomEntreprise(nomEntreprise);
						pro.setTypeEntreprise(typeEntreprise);

						clients.add(pro);
				
			}
			}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			return (Client) clients;
		}

	@Override
	public void create(Client obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO clients (client_id, adresse_id, type_client, "
							+ "moyent_paiement, civilite, nom, prenom, numero_siret, "
							+ "nom_entreprise, numero_tva, type_entreprise, mail, telephone) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");

			ps.setLong(1, obj.getId());
			ps.setNull(2, Types.NUMERIC);
			ps.setString(3, obj.getTypeClient().toString());
			ps.setString(4, obj.getMoyenPaiement().toString());
			ps.setString(12, obj.getMail());
			ps.setString(13, obj.getTelephone());

			if (obj.getTypeClient().toString() == "Particulier") {

				ClientParticulier particulier = (ClientParticulier) obj;
				ps.setString(5, particulier.getCivilite().toString());
				ps.setString(6, particulier.getNom());
				ps.setString(7, particulier.getPrenoms());

			} else {
				ps.setNull(5, Types.VARCHAR);
				ps.setNull(6, Types.VARCHAR);
				ps.setNull(7, Types.VARCHAR);
			}

			if (obj.getTypeClient().toString() == "Professionnel") {

				ClientPro pro = (ClientPro) obj;
				ps.setLong(8, pro.getNumeroSIRET());
				ps.setString(9, pro.getNomEntreprise());
				ps.setString(10, pro.getNumTVA());
				ps.setString(11, pro.getTypeEntreprise());
			} else {
				ps.setNull(8, Types.NUMERIC);
				ps.setNull(9, Types.VARCHAR);
				ps.setNull(10, Types.VARCHAR);
				ps.setNull(11, Types.VARCHAR);
			}

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Client obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("UPDATE clients SET adresse_id =?, type_client =?, "
							+ "moyent_paiement =?, civilite =?, nom =?, prenom =?, numero_siret =?, "
							+ "nom_entreprise =?, numero_tva =?, type_entreprise =?, mail =?, telephone =? "
							+ " WHERE client_d = ?");

			ps.setLong(1, obj.getId());
			ps.setNull(2, Types.NUMERIC);
			ps.setString(3, obj.getTypeClient().toString());
			ps.setString(4, obj.getMoyenPaiement().toString());
			ps.setString(12, obj.getMail());
			ps.setString(13, obj.getTelephone());

			if (obj.getTypeClient().toString() == "Particulier") {

				ClientParticulier particulier = (ClientParticulier) obj;
				ps.setString(5, particulier.getCivilite().toString());
				ps.setString(6, particulier.getNom());
				ps.setString(7, particulier.getPrenoms());

			} else {
				ps.setNull(5, Types.VARCHAR);
				ps.setNull(6, Types.VARCHAR);
				ps.setNull(7, Types.VARCHAR);
			}

			if (obj.getTypeClient().toString() == "Professionnel") {

				ClientPro pro = (ClientPro) obj;
				ps.setLong(8, pro.getNumeroSIRET());
				ps.setString(9, pro.getNomEntreprise());
				ps.setString(10, pro.getNumTVA());
				ps.setString(11, pro.getTypeEntreprise());
			} else {
				ps.setNull(8, Types.NUMERIC);
				ps.setNull(9, Types.VARCHAR);
				ps.setNull(10, Types.VARCHAR);
				ps.setNull(11, Types.VARCHAR);
			}

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(Client obj) {
		deleteById(obj.getId());

	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE clients WHERE id = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
