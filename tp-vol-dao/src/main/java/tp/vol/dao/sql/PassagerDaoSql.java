package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import tp.vol.SingletonVol;
import tp.vol.dao.IPassagerDao;
import tp.vol.model.Civilite;
import tp.vol.model.Passager;
import tp.vol.model.TypePieceIdentite;
import tp.vol.model.Ville;


public class PassagerDaoSql implements IPassagerDao {

	@Override
	public List<Passager> findAll() {
		List<Passager>passagers = new ArrayList<Passager>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT passager_id, nom, prenom, date_naissance, num_identite, nationalite, civilite, type_pi,date_validation_pi,mail,telephone FROM passagers");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long id =rs.getLong("passager_id");
				String nom = rs.getString("nom");
				String prenom= rs.getString("prenom");
				Date dtnaissance= rs.getDate("date_naissance");
				String numIdentite=rs.getString("num_identite");
				String nationalite=rs.getString("nationalite");
				String civiliteStr=rs.getString("civilite");
				String typePiStr = rs.getString("type_pi");
				Date dtValidationPi=rs.getDate("date_validation_pi");
				String telephone=rs.getString("telephone");

				Passager passager = new Passager();
				passager.setId(id);
				passager.setNom(nom);
				passager.setPrenoms(prenom);
				passager.setDtNaissance(dtnaissance);
				passager.setNumIdentite(numIdentite);
				passager.setNationalite(nationalite);
				passager.setCivilite(Civilite.valueOf(civiliteStr));
				passager.setTypePI(TypePieceIdentite.valueOf(typePiStr));
				passager.setDateValiditePI(dtValidationPi);
				passager.setTelephone(telephone);

				passagers.add(passager);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passagers;	
	}


	@Override
	public Passager findById(Long id) {
		Passager passager= new Passager();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT passager_id, nom, prenom, date_naissance, num_identite, nationalite, civilite, type_pi,date_validation_pi,mail,telephone FROM passagers WHERE passager_id = ?");

			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Long id2 =rs.getLong("passager_id");
				String nom = rs.getString("nom");
				String prenom= rs.getString("prenom");
				Date dtnaissance= rs.getDate("date_naissance");
				String numIdentite=rs.getString("num_identite");
				String nationalite=rs.getString("nationalite");
				String civiliteStr=rs.getString("civilite");
				String typePiStr = rs.getString("type_pi");
				Date dtValidationPi=rs.getDate("date_validation_pi");
				String telephone=rs.getString("telephone");

				passager.setId(id2);
				passager.setNom(nom);
				passager.setPrenoms(prenom);
				passager.setDtNaissance(dtnaissance);
				passager.setNumIdentite(numIdentite);
				passager.setNationalite(nationalite);
				passager.setCivilite(Civilite.valueOf(civiliteStr));
				passager.setTypePI(TypePieceIdentite.valueOf(typePiStr));
				passager.setDateValiditePI(dtValidationPi);
				passager.setTelephone(telephone);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passager;
	}


	@Override
	public void create(Passager obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();


			PreparedStatement ps = connection.prepareStatement("INSERT INTO passagers (passager_id, nom, prenom, date_naissance, num_identite, nationalite, civilite, type_pi, date_validation_pi ,mail ,telephone) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?)");


			ps.setLong(1, obj.getId());
			ps.setString(2, obj.getNom());
			ps.setString(3, obj.getPrenoms());
			ps.setDate(4, new Date(obj.getDtNaissance().getTime()));
			ps.setString(5,obj.getNumIdentite());
			ps.setString(6, obj.getNationalite());
			ps.setString(7,String.valueOf(obj.getCivilite()));
			ps.setString(8, String.valueOf(obj.getTypePI()));
			ps.setDate(9, new Date(obj.getDateValiditePI().getTime()));
			ps.setString(10, obj.getMail());
			ps.setString(11, obj.getTelephone());

			int rows = ps.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Passager obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();


			PreparedStatement ps = connection.prepareStatement("UPDATE  passagers SET  nom=?, prenom=?, date_naissance=?, num_identite=?, nationalite=?, civilite=?, type_pi=?, date_validation_pi=? ,mail=? ,telephone=? WHERE passager_id=? ");


			ps.setLong(11, obj.getId());
			ps.setString(1, obj.getNom());
			ps.setString(2, obj.getPrenoms());
			ps.setDate(3, new Date(obj.getDtNaissance().getTime()));
			ps.setString(4,obj.getNumIdentite());
			ps.setString(5, obj.getNationalite());
			ps.setString(6,String.valueOf(obj.getCivilite()));
			ps.setString(7, String.valueOf(obj.getTypePI()));
			ps.setDate(8, new Date(obj.getDateValiditePI().getTime()));
			ps.setString(9, obj.getMail());
			ps.setString(10, obj.getTelephone());

			int rows = ps.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(Passager obj) {
		deleteById(obj.getId());

	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE passagers WHERE passager_id = ?");

			ps.setLong(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}


