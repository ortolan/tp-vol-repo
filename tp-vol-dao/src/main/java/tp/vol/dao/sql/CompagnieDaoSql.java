package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.SingletonVol;
import tp.vol.dao.ICompagnieDao;
import tp.vol.model.Compagnie;
import tp.vol.model.Ville;

public class CompagnieDaoSql implements ICompagnieDao{

	@Override
	public List<Compagnie> findAll() {
		List<Compagnie>compagnies = new ArrayList<Compagnie>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom FROM compagnies");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String nom = rs.getString("nom");

				Compagnie compagnie = new Compagnie();
				compagnie.setNom(nom);
				compagnies.add(compagnie);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return compagnies;	
	}
	

	@Override
	public Compagnie findById(String id) {
		Compagnie compagnie = new Compagnie();
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom FROM compagnies where nom=?");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			

			while (rs.next()) {
				String nom = rs.getString("nom");
				compagnie.setNom(nom);
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return compagnie;
	}

	@Override
	public void create(Compagnie obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();
			PreparedStatement ps = connection.prepareStatement("INSERT INTO compagnies (nom) VALUES (?)");
			ps.setString(1, obj.getNom());
	
			int rows = ps.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(Compagnie obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE compagnies SET nom = ? WHERE nom = ?");
			
			ps.setString(1, obj.getNom());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void delete(Compagnie obj) {
		deleteById(obj.getNom());
		
	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE compagnies WHERE nom = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
