package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.SingletonVol;
import tp.vol.dao.IPassagerDao;
import tp.vol.dao.IVoyageDao;
import tp.vol.model.Ville;
import tp.vol.model.Voyage;

public class VoyageDaoSql implements IVoyageDao{

	@Override
	public List<Voyage> findAll() {
		List<Voyage>voyages = new ArrayList<Voyage>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT numero FROM voyages");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String nom = rs.getString("numero");

				Voyage voyage = new Voyage();
				voyage.setNumero(nom);
			
				
				voyages.add(voyage);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return voyages;
	}

	@Override
	public Voyage findById(String id) {
		Voyage voyage=new Voyage();
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT numero FROM villes WHERE numero = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String nom = rs.getString("numero");
				
				
				voyage.setNumero(id);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return voyage;
	}

	@Override
	public void create(Voyage obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();
			

			PreparedStatement ps = connection.prepareStatement("INSERT INTO voyages (numero) VALUES (?)");
			
		
			ps.setString(1, obj.getNumero());
	
			int rows = ps.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
	

	@Override
	public void update(Voyage obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();
			

			PreparedStatement ps = connection.prepareStatement("UPDATE  voyages set numero=? where numero=? ");
			
		
			ps.setString(1, obj.getNumero());
	
			int rows = ps.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Voyage obj) {
		deleteById(obj.getNumero());
		
	}

	@Override
	public void deleteById(String id) {

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE voyages WHERE numero = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}
