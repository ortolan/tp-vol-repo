package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import tp.vol.SingletonVol;
import tp.vol.dao.IReservationDao;
import tp.vol.model.Reservation;

public class ReservationDaoSql implements IReservationDao {

	@Override
	public List<Reservation> findAll() {
		List<Reservation> reservations = new ArrayList<Reservation>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT numero , statut, tarif, taux_tva, date_reservation FROM reservations");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String numero = rs.getString("numero");
				// Long passager = rs.getLong("passager_id");
				// Long client = rs.getLong("client_id");
				String statut = rs.getString("statut");
				Float tarif = rs.getFloat("tarif");
				Float tauxTVA = rs.getFloat("taux_tva");
				Date dateReservation = rs.getDate("date_reservation");
				// String voyage = rs.getString("voyage_numero");

				Reservation reservation = new Reservation();
				reservation.setNumero(numero);
//				reservation.setPassager(passager);;
//				reservation.setClient(client);
				reservation.setStatut(Boolean.valueOf(statut));
				reservation.setTarif(tarif);
				reservation.setTauxTVA(tauxTVA);
				reservation.setDateReservation(dateReservation);
				// reservation.setVoyage(voyage);

				reservations.add(reservation);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return reservations;
	}

	@Override
	public Reservation findById(String id) {
		Reservation reservation = null;

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT numero , statut, tarif, taux_tva, date_reservation FROM reservations WHERE numero = ?");

			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String numero = rs.getString("numero");
				// Long passager = rs.getLong("passager_id");
				// Long client = rs.getLong("client_id");
				String statut = rs.getString("statut");
				Float tarif = rs.getFloat("tarif");
				Float tauxTVA = rs.getFloat("taux_tva");
				Date dateReservation = rs.getDate("date_reservation");
				// String voyage = rs.getString("voyage_numero");

				reservation = new Reservation();
				reservation.setNumero(numero);
//				reservation.setPassager(passager);;
//				reservation.setClient(client);
				reservation.setStatut(Boolean.valueOf(statut));
				reservation.setTarif(tarif);
				reservation.setTauxTVA(tauxTVA);
				reservation.setDateReservation(dateReservation);
				// reservation.setVoyage(voyage);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return reservation;
	}

	@Override
	public void create(Reservation obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO reservations (numero , statut, tarif, taux_tva, date_reservation) VALUES (?,?,?,?,?)");

			// Transformation de boolean en Boolean de deux façons :
//			((Boolean)obj.isStatut()).toString()
//			Boolean.toString(b)

			ps.setString(1, obj.getNumero());
			
			if ((Boolean)obj.isStatut() == true) {
				ps.setInt(2,  1);
			} else {
				ps.setInt(2,  0);
			}
			ps.setFloat(3, obj.getTarif());
			ps.setFloat(4, obj.getTauxTVA());
			ps.setDate(5, new Date(obj.getDateReservation().getTime()));

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Reservation obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE reservations SET statut = ?, tarif = ?, taux_tva = ?, date_reservation = ? WHERE numero = ?");

			if ((Boolean)obj.isStatut() == true) {
				ps.setInt(1,  1);
			} else {
				ps.setInt(1,  0);
			}
			ps.setFloat(2, obj.getTarif());
			ps.setFloat(3, obj.getTauxTVA());
			ps.setDate(4, new Date(obj.getDateReservation().getTime()));
			ps.setString(5, obj.getNumero());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Reservation obj) {
		deleteById(obj.getNumero());
	}

	@Override
	public void deleteById(String id) {

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE reservations WHERE numero = ?");

			ps.setString(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
