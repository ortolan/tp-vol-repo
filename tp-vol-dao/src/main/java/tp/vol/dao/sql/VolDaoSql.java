package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.vol.SingletonVol;
import tp.vol.dao.IVolDao;
import tp.vol.model.Vol;
import tp.vol.model.Compagnie;

public class VolDaoSql implements IVolDao {

	@Override
	public List<Vol> findAll() {
		List<Vol> vols = new ArrayList<Vol>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT numero , date_depart, date_arrivee, ouvert, nb_places, nom_compagnie FROM vols");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String numero = rs.getString("numero");
				Date dateDepart = rs.getDate("date_depart");
				Date dateArrivee = rs.getDate("date_arrivee");
				String ouvert = rs.getString("ouvert");
				int nbPlace = rs.getInt("nb_places");
				String compagnieNom = rs.getString("nom_compagnie");

				Vol vol = new Vol();
				vol.setNumero(numero);
				vol.setDateDepart(dateDepart);
				vol.setDateArrivee(dateArrivee);
				vol.setOuvert(Boolean.valueOf(ouvert));
				vol.setNbPlace(nbPlace);
				if (compagnieNom != null) {
					Compagnie compagnie = SingletonVol.getInstance().getCompagnieDao().findById(compagnieNom);
					vol.setCompagnie(compagnie);
				};
				vols.add(vol);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return vols;
	}

	@Override
	public Vol findById(String id) {
		Vol vol = null;

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT numero , date_depart, date_arrivee, ouvert, nb_places, nom_compagnie FROM vols WHERE numero = ?");

			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String numero = rs.getString("numero");
				Date dateDepart = rs.getDate("date_depart");
				Date dateArrivee = rs.getDate("date_arrivee");
				String ouvert = rs.getString("ouvert");
				int nbPlace = rs.getInt("nb_places");
				String compagnieNom = rs.getString("nom_compagnie");

				vol = new Vol();
				vol.setNumero(numero);
				vol.setDateDepart(dateDepart);
				vol.setDateArrivee(dateArrivee);
				vol.setOuvert(Boolean.valueOf(ouvert));
				vol.setNbPlace(nbPlace);
				if (compagnieNom != null) {
					Compagnie compagnie = SingletonVol.getInstance().getCompagnieDao().findById(compagnieNom);
					vol.setCompagnie(compagnie);
				};
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return vol;
	}

	@Override
	public void create(Vol obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO vols (numero , date_depart, date_arrivee, ouvert, nb_places, nom_compagnie) VALUES (?,?,?,?,?,?)");

			ps.setString(1, obj.getNumero());
			ps.setDate(2, new Date(obj.getDateDepart().getTime()));
			ps.setDate(3, new Date(obj.getDateArrivee().getTime()));

			if ((Boolean) obj.isOuvert() == true) {
				ps.setInt(4, 1);
			} else {
				ps.setInt(4, 0);
			}

			ps.setInt(5, obj.getNbPlace());
			
			if(obj.getCompagnie() != null && obj.getCompagnie().getNom() != null) {
				ps.setString(6, obj.getCompagnie().getNom());
			} else {
				ps.setNull(6, Types.VARCHAR);
			}

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Vol obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"UPDATE vols SET date_depart = ?, date_arrivee = ?, ouvert = ?, nb_places = ?, nom_compagnie = ? WHERE numero = ?");

			ps.setDate(1, new Date(obj.getDateDepart().getTime()));
			ps.setDate(2, new Date(obj.getDateArrivee().getTime()));
			if ((Boolean) obj.isOuvert() == true) {
				ps.setInt(3, 1);
			} else {
				ps.setInt(3, 0);
			}
			ps.setInt(4, obj.getNbPlace());
			
			if(obj.getCompagnie() != null && obj.getCompagnie().getNom() != null) {
				ps.setString(5, obj.getCompagnie().getNom());
			} else {
				ps.setNull(5, Types.VARCHAR);
			}
			
			ps.setString(6, obj.getNumero());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Vol obj) {
		deleteById(obj.getNumero());

	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE vols WHERE numero = ?");

			ps.setString(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
