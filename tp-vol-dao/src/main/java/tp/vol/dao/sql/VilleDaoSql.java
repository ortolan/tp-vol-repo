package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import tp.vol.SingletonVol;
import tp.vol.dao.IVilleDao;
import tp.vol.model.Ville;

public class VilleDaoSql implements IVilleDao{
	
	
	@Override
	public List<Ville> findAll() {
		
		List<Ville>villes = new ArrayList<Ville>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom FROM villes");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String nom = rs.getString("nom");

				Ville ville = new Ville();
				ville.setNom(nom);
			
				
				villes.add(ville);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return villes;	
	}

	@Override
	public Ville findById(String id) {
		
		Ville villes = null;
		
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom FROM villes WHERE nom = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String nom = rs.getString("nom");
				
				Ville ville = new Ville();
				ville.setNom(id);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return villes;
	}
	

	@Override
	public void create(Ville obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();
			

			PreparedStatement ps = connection.prepareStatement("INSERT INTO villes (nom) VALUES (?)");
			
		
			ps.setString(1, obj.getNom());
	
			int rows = ps.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Ville obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE villes SET nom = ? WHERE nom = ?");
			
			ps.setString(1, obj.getNom());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Ville obj) {
		deleteById(obj.getNom());
	}

	@Override
	public void deleteById(String id) {

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE villes WHERE nom = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}