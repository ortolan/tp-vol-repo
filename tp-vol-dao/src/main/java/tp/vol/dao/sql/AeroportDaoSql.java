package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.SingletonVol;
import tp.vol.dao.IAeroportDao;
import tp.vol.model.Aeroport;


public class AeroportDaoSql implements IAeroportDao {

	@Override
	public List<Aeroport> findAll() {
		List<Aeroport>aeroports = new ArrayList<Aeroport>();

		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT code FROM aeroports");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String code = rs.getString("code");

				Aeroport aeroport = new Aeroport();
				aeroport.setCode(code);;


				aeroports.add(aeroport);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return aeroports;	
	}

	@Override
	public Aeroport findById(String id) {
		Aeroport aeroport=new Aeroport();
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT code FROM aeroports WHERE code=?");
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			String code = rs.getString("code");


			aeroport.setCode(code);;





		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return aeroport;	

	}

	@Override
	public void create(Aeroport obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();
			

			PreparedStatement ps = connection.prepareStatement("INSERT INTO aeroports (code) VALUES (?)");
			
		
			ps.setString(1, obj.getCode());
	
			int rows = ps.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Aeroport obj) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE aeroports SET code = ? WHERE code = ?");
			
			ps.setString(1, obj.getCode());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(Aeroport obj) {
		deleteById(obj.getCode());
	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = SingletonVol.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE aeroports WHERE code = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
