package tp.vol.dao;

import tp.vol.model.Client;

public interface IClientDao extends IDao<Client, Long> {

}
