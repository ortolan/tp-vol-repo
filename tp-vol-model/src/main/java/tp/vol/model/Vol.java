package tp.vol.model;

import java.util.ArrayList;
import java.util.Date;

public class Vol {
private Date dateDepart;
private String numero;
private Date dateArrivee;
private boolean ouvert;
private int nbPlace;
private Compagnie compagnie;
private Aeroport aeroportdepart;
private Aeroport aeroportarrivee;


private ArrayList<VoyageVol>voyageVol = new ArrayList<VoyageVol>();

public Date getDateDepart() {
	return dateDepart;
}

public void setDateDepart(Date dateDepart) {
	this.dateDepart = dateDepart;
}

public String getNumero() {
	return numero;
}

public void setNumero(String numero) {
	this.numero = numero;
}

public Date getDateArrivee() {
	return dateArrivee;
}

public void setDateArrivee(Date dateArrivee) {
	this.dateArrivee = dateArrivee;
}

public boolean isOuvert() {
	return ouvert;
}

public void setOuvert(boolean ouvert) {
	this.ouvert = ouvert;
}

public int getNbPlace() {
	return nbPlace;
}

public void setNbPlace(int nbPlace) {
	this.nbPlace = nbPlace;
}

public ArrayList<VoyageVol> getVoyageVol() {
	return voyageVol;
}

public void setVoyageVol(ArrayList<VoyageVol> voyageVol) {
	this.voyageVol = voyageVol;
}

public Compagnie getCompagnie() {
	return compagnie;
}

public void setCompagnie(Compagnie compagnie) {
	this.compagnie = compagnie;
}

public Aeroport getAeroportdepart() {
	return aeroportdepart;
}

public void setAeroportdepart(Aeroport aeroportdepart) {
	this.aeroportdepart = aeroportdepart;
}

public Aeroport getAeroportarrivee() {
	return aeroportarrivee;
}

public void setAeroportarrivee(Aeroport aeroportarrivee) {
	this.aeroportarrivee = aeroportarrivee;
}



}
