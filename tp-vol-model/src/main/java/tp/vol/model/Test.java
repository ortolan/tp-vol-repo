package tp.vol.model;

import java.util.Arrays;

public class Test {

	public static void main(String[] args) {

		Adresse adresse=new Adresse();
		Adresse adressefac= new Adresse();
		
		ClientParticulier charles=new ClientParticulier();
		charles.setAdressePrincipale(adresse);
		charles.setAdresseFacturation(adressefac);
		charles.setCivilite("M");
		charles.setNom("Magne");
		charles.setPrenoms("charles");
		charles.setMail("abc@yyy.fr");
		charles.setTelephone("0123456789");
		charles.setMoyenPaiment("cb");


		ClientPro sopra= new ClientPro();
		sopra.setNomEntreprise("SOPRA");
		sopra.setNumeroSIRET(1565);
		sopra.setNumTVA("15e23");
		sopra.setTypeEntreprise("d");
		sopra.setAdresseFacturation(adressefac);

		Passager thomas=new Passager();
		thomas.setAdressePrincipale(adresse);
		thomas.setCivilite("m");
		thomas.setMail("asee@yo.fr");
		thomas.setNationalite("fr");
		thomas.setNumIdentite("145f");
		thomas.setPrenoms("Thomas");
		thomas.setNom("baston");
		thomas.setTelephone("012346");
		thomas.setTypePI("passeport");

		Reservation resa1=new Reservation();
		resa1.setClient(charles);
		resa1.setNumero("12fr3");
		resa1.setPassager(thomas);
		resa1.setTarif(1546.5f);
		resa1.setTauxTVA(5.5f);

		thomas.getReservation().add(resa1);
		charles.getReservation().add(resa1);
		
		Voyage parisnew =new Voyage();
		resa1.setVoyage(parisnew);
		parisnew.getReservations().add(resa1);
		
		VoyageVol voyvo1= new VoyageVol();
		voyvo1.setOrdre(1);
		voyvo1.setVoyage(parisnew);

		parisnew.getVoyageVol().add(voyvo1);

		Vol parisLondre= new Vol();
		parisLondre.setNbPlace(75);
		parisLondre.setNumero(456);
		parisLondre.setOuvert(true);
		parisLondre.getVoyageVol().add(voyvo1);

		voyvo1.setVol(parisLondre);

		Compagnie airFrance=new Compagnie();
		airFrance.setNom("Air France");
		airFrance.getVol().add(parisLondre);

		parisLondre.setCompagnie(airFrance);

		Aeroport charlesdegaulle= new Aeroport();
		charlesdegaulle.setCode("pdg");
		charlesdegaulle.getVol().add(parisLondre);

		Ville paris=new Ville();
		paris.setAeroport(charlesdegaulle);
		paris.setNom("paris");

		charlesdegaulle.getVille().add(paris);

		parisLondre.setAeroportdepart(charlesdegaulle);

		Aeroport londreaero=new Aeroport();
		londreaero.setCode("lnd");
		londreaero.getVol().add(parisLondre);

		Ville londre= new Ville();
		londre.setNom("Londre");
		londre.setAeroport(londreaero);

		londreaero.getVille().add(londre);

		parisLondre.setAeroportarrivee(londreaero);

		Ville newyork=new Ville();
		newyork.setNom("new york");

		Aeroport jfk= new Aeroport();
		jfk.setCode("jfk");
		jfk.getVille().add(newyork);

		newyork.setAeroport(jfk);

		Vol londreNew=new Vol();
		londreNew.setAeroportarrivee(jfk);
		londreNew.setAeroportdepart(londreaero);
		londreNew.setCompagnie(airFrance);
		londreNew.setNbPlace(222);
		londreNew.setNumero(2);
		VoyageVol voyVol2 =new VoyageVol();
		voyVol2.setOrdre(2);
		voyVol2.setVol(londreNew);
		voyVol2.setVoyage(parisnew);

		jfk.getVol().add(londreNew);
		londreaero.getVol().add(londreNew);
		londreNew.getVoyageVol().add(voyVol2);
		parisnew.getVoyageVol().add(voyVol2);
		airFrance.getVol().add(londreNew);
		
		
		Ville stDenis=new Ville();
		stDenis.setAeroport(charlesdegaulle);
		stDenis.setNom("Saint Denis");
		
		charlesdegaulle.getVille().add(stDenis);
		
		System.out.println(charles.getReservation().get(0).getPassager().getNom() + " " + charles.getReservation().get(0).getPassager().getPrenoms());
		System.out.println(charles.getReservation().get(0).getVoyage().getVoyageVol().get(0).getVol().getCompagnie().getNom());
		
		System.out.println("BRAVO");
	}

}
