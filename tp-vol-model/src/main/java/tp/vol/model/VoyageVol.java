package tp.vol.model;


public class VoyageVol {
private int ordre;
private Voyage voyage;
private Vol vol;


public Vol getVol() {
	return vol;
}
public void setVol(Vol vol) {
	this.vol = vol;
}
public Voyage getVoyage() {
	return voyage;
}
public void setVoyage(Voyage voyage) {
	this.voyage = voyage;
}
public int getOrdre() {
	return ordre;
}
public void setOrdre(int ordre) {
	this.ordre = ordre;
}


}
