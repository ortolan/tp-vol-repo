package tp.vol.model;

public class ClientPro extends Client{
private int numeroSIRET;
private String  nomEntreprise;
private String numTVA;
private String typeEntreprise;
public int getNumeroSIRET() {
	return numeroSIRET;
}
public void setNumeroSIRET(int numeroSIRET) {
	this.numeroSIRET = numeroSIRET;
}
public String getNomEntreprise() {
	return nomEntreprise;
}
public void setNomEntreprise(String nomEntreprise) {
	this.nomEntreprise = nomEntreprise;
}
public String getNumTVA() {
	return numTVA;
}
public void setNumTVA(String numTVA) {
	this.numTVA = numTVA;
}
public String getTypeEntreprise() {
	return typeEntreprise;
}
public void setTypeEntreprise(String typeEntreprise) {
	this.typeEntreprise = typeEntreprise;
}


}
