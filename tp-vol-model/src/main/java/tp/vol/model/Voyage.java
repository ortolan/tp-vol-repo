package tp.vol.model;

import java.util.ArrayList;

public class Voyage {
//private Reservation reservation;
	private String numero;
private ArrayList<VoyageVol>voyageVol = new ArrayList<VoyageVol>();
private ArrayList<Reservation> reservations= new ArrayList<Reservation>();



public String getNumero() {
	return numero;
}

public void setNumero(String numero) {
	this.numero = numero;
}

public ArrayList<VoyageVol> getVoyageVol() {
	return voyageVol;
}

public void setVoyageVol(ArrayList<VoyageVol> voyageVol) {
	this.voyageVol = voyageVol;
}

public ArrayList<Reservation> getReservations() {
	return reservations;
}

public void setReservations(ArrayList<Reservation> reservations) {
	this.reservations = reservations;
}


}
