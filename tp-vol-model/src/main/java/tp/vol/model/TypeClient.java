package tp.vol.model;

public enum TypeClient {
	Particulier("Particulier"), Pro("Professionnel");

	private final String label;

	private TypeClient(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}
