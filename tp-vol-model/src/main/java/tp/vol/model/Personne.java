package tp.vol.model;

public abstract class Personne {
	private String mail;
	private String telephone;
	
	private Adresse adressePrincipale=new Adresse();


	public Adresse getAdressePrincipale() {
		return adressePrincipale;
	}
	public void setAdressePrincipale(Adresse adressePrincipale) {
		this.adressePrincipale = adressePrincipale;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
