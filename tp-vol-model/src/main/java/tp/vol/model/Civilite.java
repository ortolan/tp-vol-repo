package tp.vol.model;

public enum Civilite {
	Mr("Monsieur"), MME("Madame"), MLLE("Mademoiselle"), NSP("Ne se prononce pas");

	private final String label;

	private Civilite(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
