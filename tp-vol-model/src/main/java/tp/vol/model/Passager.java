package tp.vol.model;

import java.util.ArrayList;
import java.util.Date;

public class Passager extends Personne {
private Long id;
private String nom;
private String prenoms;
private Date dtNaissance;
private String numIdentite;
private String nationalite;
private Civilite civilite;
private TypePieceIdentite typePI;
private Date dateValiditePI;

private ArrayList<Reservation>reservation=new ArrayList<Reservation>();

public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenoms() {
	return prenoms;
}
public void setPrenoms(String prenoms) {
	this.prenoms = prenoms;
}
public Date getDtNaissance() {
	return dtNaissance;
}
public void setDtNaissance(Date dtNaissance) {
	this.dtNaissance = dtNaissance;
}
public String getNumIdentite() {
	return numIdentite;
}
public void setNumIdentite(String numIdentite) {
	this.numIdentite = numIdentite;
}
public String getNationalite() {
	return nationalite;
}
public void setNationalite(String nationalite) {
	this.nationalite = nationalite;
}

public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public Civilite getCivilite() {
	return civilite;
}
public void setCivilite(Civilite civilite) {
	this.civilite = civilite;
}

public TypePieceIdentite getTypePI() {
	return typePI;
}
public void setTypePI(TypePieceIdentite typePI) {
	this.typePI = typePI;
}
public Date getDateValiditePI() {
	return dateValiditePI;
}
public void setDateValiditePI(Date dateValiditePI) {
	this.dateValiditePI = dateValiditePI;
}
public ArrayList<Reservation> getReservation() {
	return reservation;
}
public void setReservation(ArrayList<Reservation> reservation) {
	this.reservation = reservation;
}



}
