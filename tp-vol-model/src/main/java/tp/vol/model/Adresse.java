package tp.vol.model;

public class Adresse {
private String voie;
private String complement;
private String codePostal;
private String ville;
private String pays;

public Adresse() {
	
}

public String getVoie() {
	return voie;
}
public void setVoie(String voie) {
	this.voie = voie;
}
public String getComplement() {
	return complement;
}
public void setComplement(String complement) {
	this.complement = complement;
}


public String getCodePostal() {
	return codePostal;
}
public void setCodePostal(String codePostal) {
	this.codePostal = codePostal;
}
public String getVille() {
	return ville;
}
public void setVille(String ville) {
	this.ville = ville;
}
public String getPays() {
	return pays;
}
public void setPays(String pays) {
	this.pays = pays;
}


}
