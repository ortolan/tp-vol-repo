package tp.vol.model;

import java.util.ArrayList;

public abstract class Client extends Personne {
private Long id;
private TypeClient typeClient;
private MoyenPaiement moyenPaiement;
private Civilite civilite;



private ArrayList<Reservation> reservation= new ArrayList<Reservation>();
private Adresse adresseFacturation=new Adresse();

public Adresse getAdresseFacturation() {
	return adresseFacturation;
}

public void setAdresseFacturation(Adresse adresseFacturation) {
	this.adresseFacturation = adresseFacturation;
}



public ArrayList<Reservation> getReservation() {
	return reservation;
}

public void setReservation(ArrayList<Reservation> reservation) {
	this.reservation = reservation;
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}


public TypeClient getTypeClient() {
	return typeClient;
}

public void setTypeClient(TypeClient typeClient) {
	this.typeClient = typeClient;
}

public MoyenPaiement getMoyenPaiement() {
	return moyenPaiement;
}

public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
	this.moyenPaiement = moyenPaiement;
}

public Civilite getCivilite() {
	return civilite;
}

public void setCivilite(Civilite civilite) {
	this.civilite = civilite;
}



}
