package tp.vol.model;

public class Ville {
private String nom;
private Aeroport Aeroport;


public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public Aeroport getAeroport() {
	return Aeroport;
}
public void setAeroport(Aeroport aeroport) {
	Aeroport = aeroport;
}


}
