package tp.vol.model;

import java.util.ArrayList;

public class Aeroport {
private String code;
private ArrayList<Vol>vol=new ArrayList<Vol>();


private ArrayList<Ville> ville =new ArrayList<Ville>();
//private Ville ville;


public String getCode() {
	return code;
}




public ArrayList<Vol> getVol() {
	return vol;
}




public void setVol(ArrayList<Vol> vol) {
	this.vol = vol;
}




public void setCode(String code) {
	this.code = code;
}




public ArrayList<Ville> getVille() {
	return ville;
}




public void setVille(ArrayList<Ville> ville) {
	this.ville = ville;
}






}
