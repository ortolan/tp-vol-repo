package tp.vol.model;

import java.util.Date;

public class Reservation {
private String numero;
private boolean statut;
private float tarif;
private float tauxTVA;
private Date dateReservation;

private Client client;
private Passager passager;

private Voyage voyage;


public String getNumero() {
	return numero;
}
public void setNumero(String numero) {
	this.numero = numero;
}

public boolean isStatut() {
	return statut;
}
public void setStatut(boolean statut) {
	this.statut = statut;
}

public float getTarif() {
	return tarif;
}
public void setTarif(float tarif) {
	this.tarif = tarif;
}

public float getTauxTVA() {
	return tauxTVA;
}
public void setTauxTVA(float tauxTVA) {
	this.tauxTVA = tauxTVA;
}

public Date getDateReservation() {
	return dateReservation;
}

public void setDateReservation(Date dateReservation) {
	this.dateReservation = dateReservation;
}

public Client getClient() {
	return client;
}
public void setClient(Client client) {
	this.client = client;
}

public Passager getPassager() {
	return passager;
}
public void setPassager(Passager passager) {
	this.passager = passager;
}

public Voyage getVoyage() {
	return voyage;
}
public void setVoyage(Voyage voyage) {
	this.voyage = voyage;
}


}
